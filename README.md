# **WRITE-UP TP-EXAM CCNA TER**


## **INFOS**


### **COMPUTERS**

#### ANGELA
- IP: ``10.0.10.1``
- Mask: ``255.255.255.0``
- Broadcast: ``10.0.10.254``
- Address-range: ``10.0.10.0/24``
- VLAN: ``10``
- VLAN-Name: ``QWERTY``

#### JOANNA
- IP: ``10.0.10.2``
- Mask: ``255.255.255.0``
- Broadcast: ``10.0.10.254``
- Address-range: ``10.0.10.0/24``
- VLAN: ``10``
- VLAN-Name: ``QWERTY``

#### TYRELL
- IP: ``10.0.20.1``
- Mask: ``255.255.255.0``
- Broadcast: ``10.0.20.254``
- Address-range: ``10.0.20.0/24``
- VLAN: ``20``
- VLAN-Name: ``FTPonly``

#### MR PRICE
- IP: ``10.0.20.2``
- Mask: ``255.255.255.0``
- Broadcast: ``10.0.20.254``
- Address-range: ``10.0.20.0/24``
- VLAN: ``20``
- VLAN-Name: ``FTPonly``

#### PHILLIP
- IP: ``10.0.80.1``
- Mask: ``255.255.255.0``
- Broadcast: ``10.0.80.254``
- Address-range: ``10.0.80.0/24``
- VLAN: ``80``
- VLAN-Name: ``Natif/Admin``

#### DARLENE
- IP: ``192.168.0.1``
- Mask: ``255.255.255.0``
- Broadcast: ``192.168.0.254``
- Address-range: ``192.168.0.0/24``

#### ELLIOT
- IP: ``192.168.0.2``
- Mask: ``255.255.255.0``
- Broadcast: ``192.168.0.254``
- Address-range: ``192.168.0.0/24``

#### BUREAUX (NTP/FTP/SYSLOG/Radius)
- IP: ``10.0.80.10``
- Mask: ``255.255.255.0``
- Broadcast: ``10.0.80.254``
- Address-range: ``10.0.80.0/24``

#### RUDY
- IP: ``10.10.100.222``
- Mask: ``255.255.255.0``
- Broadcast: ``10.10.100.254``
- Address-range: ``10.10.100.0/24``

### **ROUTERS**

#### RouterIntra
- Interface G0/0.0
- Interface G0/0.10
	- IP: ``10.0.10.254``
	- Mask: ``255.255.255.0``
- Interface G0/0.20
	- IP: ``10.0.20.254``
	- Mask: ``255.255.255.0``
- Interface G0/0.80
	- IP: ``10.0.80.254``
	- Mask: ``255.255.255.0``
- Interface G0/1
	- IP: ``172.31.200.1``
	- Mask: ``255.255.255.252``

#### RouterFW
- Interface G0/0
- Interface G0/1

---

## **COMMANDS**

### **ROUTERS**

#### **RouterIntra**
##### Hostname
```
en
conf t

hostname RouterIntra
no ip domain-lookup

end
wr

```
##### IP Adresses
```
en
conf t

int range g0/0-2
shutdown
exit

int g0/1
ip address 172.31.200.1 255.255.255.252
no shutdown
exit

end
wr

```
##### Routage inter-vlan (router-on-a-stick)
```
en
conf t

int g0/0
no shutdown
exit
int g0/0.10
description QWERTY VLAN
encapsulation dot1Q 10
ip address 10.0.10.254 255.255.255.0
no shutdown
exit
int g0/0.20
description FTPonly VLAN
encapsulation dot1Q 20
ip address 10.0.20.254 255.255.255.0
no shutdown
exit
int g0/0.80
description Natif/Admin VLAN
encapsulation dot1Q 80 native
ip address 10.0.80.254 255.255.255.0
no shutdown
exit

end
wr

```
##### SYSLOG LOGGING + NTP
```
en
conf t

no cdp run
logging trap debugging
logging host 10.0.80.10
clock timezone CET 1
ntp server 10.0.80.10

end
wr

```

---

#### **RouterFW**
##### Hostname
```
en
conf t

hostname RouterFW
no ip domain-lookup

end
wr

```
##### IP Adresses
```
en
conf t

int g0/1
ip address 172.31.200.2 255.255.255.252
no shutdown
exit
int g0/0
ip address 172.31.100.1 255.255.255.252
no shutdown
exit
int g0/2
ip address 10.10.100.254 255.255.255.0
no shutdown
exit

end
wr

```

---

#### **E-Router**
##### Hostname
```
en
conf t

hostname E-Router
no ip domain-lookup

end
wr

```
##### IP Adresses
```
en
conf t

int g0/0
ip address 172.31.100.2 255.255.255.252
no shutdown
exit


end
wr

```

---

#### **fsociety**
##### Hostname
```
en
conf t

hostname fsociety
no ip domain-lookup

end
wr

```
##### IP Adresses
```
en
conf t

int g0/0
ip address 192.168.0.255 255.255.255.0
no shutdown
exit


end
wr

```

---

### **SWITCHES**

#### **E-Switch0**
##### Hostname
```
en
conf t

hostname E-Switch0
no ip domain-lookup

end
wr

```
##### VLANs
```
en
conf t

vlan 10
name QWERTY
exit
vlan 20
name FTPonly
exit
vlan 80
name Natif/Admin
exit

int range f0/1-24, g0/1-2
shutdown
exit

int fa0/24
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
spanning-tree portfast trunk
exit
int g0/1
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
exit
int g0/2
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
exit

end
wr

```
##### NTP
```
en
conf t

no cdp run
clock timezone CET 1
ntp server 10.0.80.10

end
wr

```

---

#### **E-Switch1**
##### Hostname
```
en
conf t

hostname E-Switch1
no ip domain-lookup

end
wr

```
##### VLANs
```
en
conf t

vlan 10
name QWERTY
exit
vlan 20
name FTPonly
exit
vlan 80
name Natif/Admin
exit

int range f0/1-24, g0/1-2
shutdown
exit

int fa0/11
no shutdown
switchport mode access
switchport access vlan 20
switchport port-security mac-address sticky
switchport port-security violation restrict
switchport port-security
spanning-tree portfast
spanning-tree bpduguard enable
exit
int fa0/1
no shutdown
switchport mode access
switchport access vlan 10
switchport port-security mac-address sticky
switchport port-security violation restrict
switchport port-security
spanning-tree portfast
spanning-tree bpduguard enable
exit
int fa0/3
no shutdown
switchport mode access
switchport access vlan 80
switchport port-security mac-address sticky
switchport port-security violation restrict
switchport port-security
spanning-tree portfast
spanning-tree bpduguard enable
exit

int g0/1
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
exit
int g0/2
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
exit

end
wr

```
##### NTP
```
en
conf t

no cdp run
clock timezone CET 1
ntp server 10.0.80.10

end
wr

```

---

#### **E-Switch2**
##### Hostname
```
en
conf t

hostname E-Switch2
no ip domain-lookup

end
wr

```
##### VLANs
```
en
conf t

vlan 10
name QWERTY
exit
vlan 20
name FTPonly
exit
vlan 80
name Natif/Admin
exit

int range f0/1-24, g0/1-2
shutdown
exit

int fa0/20
no shutdown
switchport mode access
switchport access vlan 80
switchport port-security mac-address sticky
switchport port-security violation restrict
switchport port-security
spanning-tree portfast
spanning-tree bpduguard enable
exit
int fa0/10
no shutdown
switchport mode access
switchport access vlan 10
switchport port-security mac-address sticky
switchport port-security violation restrict
switchport port-security
spanning-tree portfast
spanning-tree bpduguard enable
exit
int fa0/2
no shutdown
switchport mode access
switchport access vlan 20
switchport port-security mac-address sticky
switchport port-security violation restrict
switchport port-security
spanning-tree portfast
spanning-tree bpduguard enable
exit

int g0/1
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
exit
int g0/2
no shutdown
switchport mode trunk
switchport trunk allowed vlan 10,20,80
switchport trunk native vlan 80
exit

end
wr

```
##### NTP
```
en
conf t

no cdp run
clock timezone CET 1
ntp server 10.0.80.10

end
wr

```

---

#### **DMZ-Switch**
##### Hostname
```
en
conf t

hostname DMZ-Switch
no ip domain-lookup

end
wr

```
##### VLANs
```
en
conf t

int range f0/1-24, g0/1-2
shutdown
exit

int f0/2
no shutdown
exit
int g0/2
no shutdown
exit

end
wr

```

---

#### **FS-Switch**
##### Hostname
```
en
conf t

hostname FS-Switch
no ip domain-lookup

end
wr

```
##### VLANs
```
en
conf t

int range f0/1-24, g0/1-2
shutdown
exit

int range f0/1-2
no shutdown
exit
int g0/1
no shutdown
exit

end
wr

```

---